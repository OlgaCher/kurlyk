package kurlyk.models;


public enum State {
    ACTIVE, BANNED, DELETED;
}
