package kurlyk.models;


public enum Role {
    ADMIN, PROFESSOR, STUDENT
}
