package kurlyk.view.create.commonCreateWindow;

import kurlyk.view.common.stage.base.BaseStage;

public class CommonCreateStage extends BaseStage<CommonCreateController> {


    public CommonCreateStage() {
        super();
    }

    @Override
    public String getPathToMainStage() {
        return "create/commonCreateWindow/main";
    }
}
