package kurlyk.view.create.createLabWorkWindow;

import kurlyk.view.common.stage.base.BaseStage;

public class CreateLabWorkStage extends BaseStage<CreateLabWorkController> {


    public CreateLabWorkStage() {
        super();
    }

    @Override
    public String getPathToMainStage() {
        return "create/createLabWorkWindow/main";
    }
}
