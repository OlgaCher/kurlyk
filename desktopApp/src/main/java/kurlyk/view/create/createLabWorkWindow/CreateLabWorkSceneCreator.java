package kurlyk.view.create.createLabWorkWindow;

import kurlyk.view.common.stage.base.BaseSceneCreator;

public class CreateLabWorkSceneCreator extends BaseSceneCreator<CreateLabWorkController> {


    public CreateLabWorkSceneCreator() {
        super();
    }

    @Override
    public String getPathToMainStage() {
        return "create/CreateLabWorkWindow/main";
    }
}
